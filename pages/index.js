import { useState } from 'react';
import axios from 'axios';

export default function Home() {
  const [videoUrl, setVideoUrl] = useState('');
  const [downloadUrl, setDownloadUrl] = useState('');

  const downloadFile = async (url, fileName) => {
    const response = await fetch(url);
    const fileBlob = await response.blob();
    const downloadUrl = URL.createObjectURL(fileBlob);
  
    const link = document.createElement('a');
    link.href = downloadUrl;
    link.download = fileName;
    link.click();
  
    URL.revokeObjectURL(downloadUrl);
  };
  
  const handleDownload = async () => {
    try {
      const response = await axios.get(
        `https://api.vk.com/method/video.get?videos=${videoUrl}&access_token=24944470249444702494447023278036b42249424944470401744fd59b7b19df355747b&v=5.131`
      );
  
      console.log(response)
      const videoData = response.data.response.items[0];
      const videoId = videoData.id;
      const ownerId = videoData.owner_id;
      const accessKey = videoData.access_key;
  
      const downloadUrl = `https://vk.com/video${ownerId}_${videoId}_${accessKey}`;
  
      downloadFile(downloadUrl, `video_${videoId}.mp4`);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div>
      <h1>VK Video Downloader</h1>
      <input
        type="text"
        placeholder="Enter VK Video URL"
        value={videoUrl}
        onChange={(e) => setVideoUrl(e.target.value)}
      />
      <button onClick={handleDownload}>Download</button>
      {downloadUrl && (
        <div>
          <h2>Download Link:</h2>
          <a href={downloadUrl} download>Download Video</a>
        </div>
      )}
    </div>
  );
}
